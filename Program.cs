﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Forever
{

    public delegate void OnExit();
    class Program
    {

        
        static public bool exit;
        static private Process p;

        

        static void Main(string[] args)
        {
            exit = false;
            var process = args[0];
            run(process);
            while (true)
            {
                Process[] pname = Process.GetProcessesByName("Panacea");
                if (pname.Length == 0)
                {
                    run(process);
                }
                Thread.Sleep(5000);
            }
        }


        public static void run(string process)
        {
            p = Process.Start(process);

            p.Exited += new EventHandler(delegate(object sender,System.EventArgs e)
            {
                if (!exit)
                {
                    Console.WriteLine("re opening {0}",process);
                    run(process);   
                };
            });

        }
    }
}
